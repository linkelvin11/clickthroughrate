import csv
import numpy as np
import timeit

# train_rev2.csv has 4769401 data points
# train_small.csv has 199229 data points
# train_smallest.csv has 99 data points

# file io
fname = "train_small.csv"
if fname == "train_small.csv":
    f_len = 199229
else:
    if fname ==  "train_smallest.csv":
        f_len = 99
    else:
        if fname == "train_rev2.csv":
            f_len = 4769401
        else:
            f_len = 0
            
file_in = open(fname)
csv_reader = csv.reader(file_in)
header = next(csv_reader)
targets = np.array([]).reshape(0,1)
features = np.array([]).reshape(0,13)
#row = next(csv_reader)
#print(row[2:5]+row[16:18]+row[19:27])

# iterate over each row and read in data
start = timeit.timeit()
for row in csv_reader:
    targets = np.vstack([targets,row[1]])
    features = np.vstack([features,row[2:5]+row[16:18]+row[19:27]])
end = timeit.timeit()
file_in.close()
out = np.hstack((targets,features))
print(out.shape)
np.savetxt("./features_small.csv",out,fmt='%s',delimiter=',')
print("it took ",end-start," seconds to read in data")

# create and train model
classifier = svm.SVC()
classifier.fit(features,targets)

# model to preduct outputs
